package com.example.googleapisyoutube;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.googleapisyoutube.Adapter.MainActivityAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private AlertDialog.Builder dialogPencarian;
    private View dialogView;
    private LayoutInflater inflater;

    private RecyclerView rvResult;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;

    private ImageButton btnCari;
    private EditText edtParams;

    String BASE_URL = "https://www.googleapis.com/youtube/v3/search?part=snippet&key=AIzaSyDH3naOGPlOL175VfhVaRrzr0438MymNxM&q=";
    String URI_REQ = "";
    String params = "";

    ArrayList<HashMap<String, String>> list_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtParams = (EditText) findViewById(R.id.edtParams);
        btnCari = (ImageButton) findViewById(R.id.btnCariResult);

        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                params = edtParams.getText().toString();
                onGetData();
            }
        });

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void onGetData() {
        rvResult = (RecyclerView) findViewById(R.id.recycleBottom);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvResult.setLayoutManager(linearLayoutManager);

        requestQueue = Volley.newRequestQueue(MainActivity.this);
        list_data = new ArrayList<HashMap<String, String>>();

        URI_REQ = BASE_URL + params;
//        Toast.makeText(getApplicationContext(), URI_REQ, Toast.LENGTH_SHORT).show();

        stringRequest = new StringRequest(Request.Method.GET, URI_REQ, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("items");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("snippet");

                        JSONObject jsonObjectImage = jsonObject2.getJSONObject("thumbnails");
                        JSONObject jsonObjectImage1 = jsonObjectImage.getJSONObject("high");
                        HashMap<String, String> hashMap = new HashMap<String, String>();

                        //kolom-kolom
                        hashMap.put("url", jsonObjectImage1.getString("url"));
                        hashMap.put("title", jsonObject2.getString("title"));
                        hashMap.put("publishedAt", jsonObject2.getString("publishedAt"));
                        hashMap.put("channelTitle", jsonObject2.getString("channelTitle"));
                        hashMap.put("description", jsonObject2.getString("description"));

                        list_data.add(hashMap);
                        MainActivityAdapter mainActivityAdapter = new MainActivityAdapter(MainActivity.this, list_data);
                        rvResult.setAdapter(mainActivityAdapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(stringRequest);
    }
}
