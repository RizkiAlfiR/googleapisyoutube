package com.example.googleapisyoutube.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.googleapisyoutube.MainActivity;
import com.example.googleapisyoutube.R;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivityAdapter extends RecyclerView.Adapter<MainActivityAdapter.ViewHolder> {
    Context context;
    ArrayList<HashMap<String, String>> list_data;

    public MainActivityAdapter(MainActivity mainActivity, ArrayList<HashMap<String, String>> list_data) {
        this.context = mainActivity;
        this.list_data = list_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_result, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Glide.with(context)
                .load(list_data.get(i).get("url"))
                .crossFade()
                .placeholder(R.drawable.picture)
                .into(viewHolder.imgResult);
        viewHolder.imgDummy.setImageResource(R.drawable.ic_user_dummy);
        viewHolder.txtTitle.setText(list_data.get(i).get("title"));
        viewHolder.txtPubblished.setText(list_data.get(i).get("publishedAt"));
        viewHolder.txtChannel.setText(list_data.get(i).get("channelTitle"));
        viewHolder.txtDescription.setText(list_data.get(i).get("description"));
    }

    @Override
    public int getItemCount() {
        return list_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imgResult, imgDummy;
        TextView txtTitle, txtPubblished, txtChannel, txtDescription;

        public ViewHolder(View itemView) {
            super(itemView);

            imgResult = (ImageView) itemView.findViewById(R.id.imgResult);
            imgDummy = (ImageView) itemView.findViewById(R.id.imgDummy);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtPubblished = (TextView) itemView.findViewById(R.id.txtPublished);
            txtChannel = (TextView) itemView.findViewById(R.id.txtChannel);
            txtDescription = (TextView) itemView.findViewById(R.id.txtDescription);
        }
    }
}
